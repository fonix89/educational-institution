package com.mapsa.springeducationalsystem.model.entity;

public enum AttendanceStatus {
    PRESENCE,
    ABSENCE
}
