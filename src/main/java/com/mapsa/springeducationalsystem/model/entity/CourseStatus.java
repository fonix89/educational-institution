package com.mapsa.springeducationalsystem.model.entity;

public enum CourseStatus {
    ON_GOING,
    ON_START,
    ON_STOP
}
