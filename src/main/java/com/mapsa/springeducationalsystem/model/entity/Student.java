package com.mapsa.springeducationalsystem.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@NoArgsConstructor
@Table(name = "STUDENT")
@Data
@AllArgsConstructor
public class Student extends User{

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "student_course",
            joinColumns = {@JoinColumn(name = "STUDENT_ID", referencedColumnName = "ID")},
            inverseJoinColumns = {@JoinColumn(name = "COURSE_ID", referencedColumnName = "ID")})
    private List<Course> studentCourseList = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "student")
    private List<Attendance> attendanceList;


}
