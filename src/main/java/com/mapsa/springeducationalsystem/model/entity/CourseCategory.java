package com.mapsa.springeducationalsystem.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "COURSECATEGORY")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CourseCategory{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column()
    private String categoryName;
}
