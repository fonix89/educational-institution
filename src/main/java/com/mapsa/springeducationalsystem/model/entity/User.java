package com.mapsa.springeducationalsystem.model.entity;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.util.Date;


@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "USER")
public class User{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NonNull
    @Column
    private String firstName;

    @NotNull
    @Column
    private String lastName;

    @NotNull
    @Column(unique = true)
    private String userName;

    @NotNull
    @Column
    private String password;

    @Column
    private String email;

    @NotNull
    @Column
    private String phoneNumber;

    @Column
    private String nationalCode;

    @Column
    private String introducedBy;

    @Column
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date birthDate;

    @Embedded
    private Address address;

    @Enumerated(value = EnumType.STRING)
    private Role role;

    @Enumerated(value = EnumType.STRING)
    private Acquainted acquaintedWays;

}
