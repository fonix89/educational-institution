package com.mapsa.springeducationalsystem.model.entity;


public enum Role {

    ADMIN,
    STUDENT,
    TEACHER,

}
