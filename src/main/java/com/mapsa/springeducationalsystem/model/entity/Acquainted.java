package com.mapsa.springeducationalsystem.model.entity;

public enum Acquainted {
    TV_ADDS,
    TELEGRAM_ADDS,
    INSTAGRAM_ADDS,
    WEB_SITES,
    OTHERS
}
