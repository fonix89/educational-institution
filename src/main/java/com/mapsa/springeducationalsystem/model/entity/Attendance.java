package com.mapsa.springeducationalsystem.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "ATTANDANCE")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Attendance {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private long id;

    @DateTimeFormat(style = "yyyy-MM-dd")
    @Column
    private Date date;

    @Column
    @Enumerated(value = EnumType.STRING)
    private AttendanceStatus attendanceStatus;

    @Column
    private String description;

    @ManyToOne
    @JoinColumn(name = "studentId")
    private Student student;

    @ManyToOne
    @JoinColumn(name = "courseId")
    private Course course;
}
