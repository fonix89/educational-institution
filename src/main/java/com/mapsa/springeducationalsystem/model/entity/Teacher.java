package com.mapsa.springeducationalsystem.model.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "TEACHER")
@NoArgsConstructor
@Data
@AllArgsConstructor
public class Teacher extends User {

//    @ManyToMany(cascade = CascadeType.ALL)
//    @JoinTable(name = "teacher_course",
//            joinColumns = {@JoinColumn(name = "TEACHER_ID", referencedColumnName = "ID")},
//            inverseJoinColumns = {@JoinColumn(name = "COURSE_ID", referencedColumnName = "ID")})
//    private List<Course> teacherCourseList = new ArrayList<>();

    @OneToMany(mappedBy = "teacher")
    private List<Course> teacherCourseList = new ArrayList<>();

}
