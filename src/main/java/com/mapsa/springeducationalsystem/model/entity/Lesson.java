package com.mapsa.springeducationalsystem.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "LESSON")
public class Lesson{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column()
    private String lessonName;

    @Column()
    private int minAllowedAge;

    @Column()
    private int maxAllowedAge;





    //private List<Lesson> pre_requirements;

}
