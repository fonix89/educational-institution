package com.mapsa.springeducationalsystem.model.entity;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Entity
@Table(name = "COURSE")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Course{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    private String courseName;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column
    private Date startDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column
    private Date endDate;

    @Column
    private int totalHours;

    @Column
    private long tuitionFee;

    @Column
    private String description;

    @Column
    private int capacity;

    @Column
    private int minAllowedAge;

    @Column
    private int maxAllowedAge;

    @Enumerated(value = EnumType.STRING)
    private CourseStatus courseStatus;

    @OneToOne
    private CourseCategory courseCategory;

    @OneToOne
    private Lesson lesson;

    @ManyToOne
    @JoinColumn(name = "TEACHER_ID", referencedColumnName = "ID")
    private Teacher teacher;

    @ManyToMany(mappedBy = "studentCourseList", cascade = CascadeType.ALL)
    private List<Student> studentList = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "course")
    private List<Attendance> attendanceList = new ArrayList<>();



}
