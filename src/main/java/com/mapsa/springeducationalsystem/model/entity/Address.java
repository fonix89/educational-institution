package com.mapsa.springeducationalsystem.model.entity;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Table;



@Embeddable
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Address{

    @JsonProperty
    @Column
    private String country;

    @JsonProperty
    @Column
    private String province;

    @JsonProperty
    @Column
    private String city;

    @JsonProperty
    @Column
    private String postalCode;


}
