package com.mapsa.springeducationalsystem.model.mapper;

import com.mapsa.springeducationalsystem.model.dto.CourseDto;
import com.mapsa.springeducationalsystem.model.entity.Course;
import com.mapsa.springeducationalsystem.model.entity.CourseCategory;
import com.mapsa.springeducationalsystem.model.entity.Lesson;
import com.mapsa.springeducationalsystem.model.entity.Teacher;
import com.mapsa.springeducationalsystem.service.TeacherService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper(componentModel = "spring", uses = {CourseCategoryMapper.class, LessonMapper.class, TeacherMapper.class})
public interface CourseMapper {

    @Mappings({
            @Mapping(source = "courseCategory.id", target = "courseCategoryId"),
            @Mapping(source = "lesson.id", target = "lessonId"),
            @Mapping(source = "teacher.id", target = "teacherId")
    })
    CourseDto courseToCourseDto(Course course);

    @Mappings({
            @Mapping(source = "courseCategoryId", target = "courseCategory.id"),
            @Mapping(source = "lessonId", target = "lesson.id"),
            @Mapping(source = "teacherId", target = "teacher.id")
    })
    Course courseDtoToCourse(CourseDto courseDto);

    List<CourseDto> courseListToCourseDtoList(List<Course> courses);


    @Mappings({
            @Mapping(source = "courseCategoryId", target = "courseCategory.id"),
            @Mapping(source = "lessonId", target = "lesson.id"),
            @Mapping(source = "teacherId", target = "teacher.id")
    })
    void updateCourseFromDto(CourseDto courseDto, @MappingTarget Course course);



}
