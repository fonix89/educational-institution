package com.mapsa.springeducationalsystem.model.mapper;

import com.mapsa.springeducationalsystem.model.dto.GetCoursesOfATeacherResponseDto;
import com.mapsa.springeducationalsystem.model.dto.TeacherDto;
import com.mapsa.springeducationalsystem.model.entity.Teacher;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring", uses = {CourseMapper.class})
public interface TeacherMapper extends UserBaseMapper<TeacherDto, Teacher> {

    Teacher teacherDtoToTeacher(TeacherDto teacherDto);
    TeacherDto teacherToTeacherDto(Teacher teacher);

    @Mappings({
            @Mapping(source = "id", target = "teacherId"),
            @Mapping(source = "teacherCourseList", target = "courses")
    })
    GetCoursesOfATeacherResponseDto getCoursesOfATeacherResponseDtoConvert(Teacher teacher);

    void updateTeacherFromDto(TeacherDto teacherDto, @MappingTarget Teacher teacher);
}
