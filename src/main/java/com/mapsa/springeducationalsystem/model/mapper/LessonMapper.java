package com.mapsa.springeducationalsystem.model.mapper;

import com.mapsa.springeducationalsystem.model.dto.LessonDto;
import com.mapsa.springeducationalsystem.model.entity.Lesson;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface LessonMapper {

    LessonMapper INSTANCE = Mappers.getMapper(LessonMapper.class);

    LessonDto lessonToLessonDto(Lesson lesson);
    Lesson lessonDtoToLesson(LessonDto lessonDto);

    void updateLessonFromDto(LessonDto lessonDto, @MappingTarget Lesson lesson);
}
