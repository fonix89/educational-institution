package com.mapsa.springeducationalsystem.model.mapper;

import com.mapsa.springeducationalsystem.model.dto.CourseCategoryDto;
import com.mapsa.springeducationalsystem.model.entity.CourseCategory;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface CourseCategoryMapper {

    CourseCategoryDto courseCategoryToCourseCategoryDto(CourseCategory courseCategory);

    CourseCategory courseCategoryDtoToCourseCatergory(CourseCategoryDto courseCategoryDto);
}
