package com.mapsa.springeducationalsystem.model.mapper;

import com.mapsa.springeducationalsystem.model.dto.AttendanceDto;
import com.mapsa.springeducationalsystem.model.dto.GetListOfStudentsAttendanceInACourseDto;
import com.mapsa.springeducationalsystem.model.entity.Attendance;
import com.mapsa.springeducationalsystem.model.entity.Course;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring", uses = {CourseMapper.class, StudentMapper.class, AttendanceMapper.class})
public interface AttendanceMapper {


    @Mappings({
            @Mapping(source = "student.id", target = "studentId"),
            @Mapping(source = "course.id", target = "courseId")
    })
    AttendanceDto AttendanceToAttendanceDto(Attendance attendance);

    @Mappings({
            @Mapping(source = "studentId", target = "student.id"),
            @Mapping(source = "courseId", target = "course.id")
    })
    Attendance AttendanceDtoToAttendance(AttendanceDto attendanceDto);

    @Mapping(source = "attendanceList", target = "attendanceList")
    GetListOfStudentsAttendanceInACourseDto getListOfStudentsAttendanceInACourseConverter(Course course);

    List<AttendanceDto> getAListOfAStudentAttendanceInACourseConverter(List<Attendance> attendances);



}
