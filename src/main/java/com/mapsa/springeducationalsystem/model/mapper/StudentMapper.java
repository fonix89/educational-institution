package com.mapsa.springeducationalsystem.model.mapper;

import com.mapsa.springeducationalsystem.model.dto.GetCoursesOfAStudentResponseDto;
import com.mapsa.springeducationalsystem.model.dto.GetListOfStudentsOfACourseResponseDto;
import com.mapsa.springeducationalsystem.model.dto.StudentDto;
import com.mapsa.springeducationalsystem.model.entity.Course;
import com.mapsa.springeducationalsystem.model.entity.Student;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;


@Mapper(componentModel = "spring", uses = {CourseMapper.class})
public interface StudentMapper extends UserBaseMapper<StudentDto, Student> {


    Student studentDtoToStudent(StudentDto studentDto);

    StudentDto studentToStudentDto(Student student);

    @Mappings({
            @Mapping(source = "id", target = "studentId"),
            @Mapping(source = "studentCourseList", target = "courses")}
    )
    GetCoursesOfAStudentResponseDto getCoursesOfAStudentResponseDtoConvert(Student student);

    @Mappings({
            @Mapping(source = "id", target = "courseId"),
            @Mapping(source = "studentList", target = "students")}
    )
    GetListOfStudentsOfACourseResponseDto getListOfStudentsOfACourseResponseDtoConvert(Course course);

    void updateStudentFromDto(StudentDto studentDto, @MappingTarget Student student);




}
