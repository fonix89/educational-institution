package com.mapsa.springeducationalsystem.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LessonDto {

    @JsonProperty(value = "lesson_name")
    private String lessonName;

    @JsonProperty(value = "min_allowed_age")
    private int minAllowedAge;

    @JsonProperty(value = "max_allowed_age")
    private int maxAllowedAge;
}
