package com.mapsa.springeducationalsystem.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mapsa.springeducationalsystem.model.entity.Acquainted;
import com.mapsa.springeducationalsystem.model.entity.Address;
import com.mapsa.springeducationalsystem.model.entity.Role;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserBaseDto {

    @JsonProperty(value = "first_name")
    private String firstName;

    @JsonProperty(value = "last_name")
    private String lastName;

    @JsonProperty(value = "user_name")
    private String userName;

    @JsonProperty(value = "password")
    private String password;

    @JsonProperty(value = "phone_number")
    private String phoneNumber;

    @JsonProperty(value = "email")
    private String email;

    @JsonProperty(value = "national_code")
    private String nationalCode;

    @JsonProperty(value = "introduced_by")
    private String introducedBy;

    @JsonProperty(value = "birth_date")
    private Date birthDate;

    @JsonProperty(value = "address")
    private Address address;

    @JsonProperty(value = "role")
    private Role role;

    @JsonProperty(value = "acquainted_ways")
    private Acquainted acquaintedWays;

}
