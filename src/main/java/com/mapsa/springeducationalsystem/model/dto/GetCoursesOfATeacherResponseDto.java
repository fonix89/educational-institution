package com.mapsa.springeducationalsystem.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetCoursesOfATeacherResponseDto {

    @JsonProperty("teacher_id")
    private long teacherId;

    @JsonProperty("course_list")
    private List<CourseDto> courses;
}
