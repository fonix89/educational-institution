package com.mapsa.springeducationalsystem.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetListOfStudentsOfACourseResponseDto {

    @JsonProperty("course_id")
    private long courseId;

    @JsonProperty("student_list")
    private List<StudentDto> students;
}
