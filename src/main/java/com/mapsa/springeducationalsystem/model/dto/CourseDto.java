package com.mapsa.springeducationalsystem.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mapsa.springeducationalsystem.model.entity.CourseCategory;
import com.mapsa.springeducationalsystem.model.entity.CourseStatus;
import com.mapsa.springeducationalsystem.model.entity.Lesson;
import com.mapsa.springeducationalsystem.model.entity.Teacher;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CourseDto {

   // private CourseCategory course_category;

    @JsonProperty(value = "course_name")
    private String courseName;

    @JsonProperty(value = "start_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date startDate;

    @JsonProperty(value = "end_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endDate;

    @JsonProperty(value = "total_hours")
    private int totalHours;

    @JsonProperty(value = "tuition_fee")
    private long tuitionFee;

    @JsonProperty(value = "description")
    private String description;

    @JsonProperty(value = "capacity")
    private int capacity;

    @JsonProperty(value = "course_status")
    private CourseStatus courseStatus;

    @JsonProperty(value = "min_allowed_age")
    private int minAllowedAge;

    @JsonProperty(value = "max_allowed_age")
    private int maxAllowedAge;

    @JsonProperty(value = "course_category_id")
    private long courseCategoryId;

    @JsonProperty(value = "lesson_id")
    private long lessonId;

    @JsonProperty(value = "teacher_id")
    private long teacherId;

}
