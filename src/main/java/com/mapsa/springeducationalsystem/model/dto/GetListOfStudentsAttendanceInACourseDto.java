package com.mapsa.springeducationalsystem.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class GetListOfStudentsAttendanceInACourseDto {

    @JsonProperty("attendance_list")
    private List<AttendanceDto> attendanceList;
}
