package com.mapsa.springeducationalsystem.model.dto;


import com.fasterxml.jackson.annotation.JsonProperty;
import com.mapsa.springeducationalsystem.model.entity.Course;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetCoursesOfAStudentResponseDto {

    @JsonProperty("student_id")
    private long studentId;

    @JsonProperty("course_list")
    private List<CourseDto> courses;
}
