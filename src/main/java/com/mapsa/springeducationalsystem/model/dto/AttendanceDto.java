package com.mapsa.springeducationalsystem.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mapsa.springeducationalsystem.model.entity.AttendanceStatus;
import com.mapsa.springeducationalsystem.model.entity.Course;
import com.mapsa.springeducationalsystem.model.entity.Student;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AttendanceDto {

    @JsonProperty(value = "student_id")
    private long studentId;

    @JsonProperty(value = "course_id")
    private long courseId;

    @JsonProperty(value = "date")
    private Date date;

    @JsonProperty(value = "attendance_status")
    private AttendanceStatus attendanceStatus;

    @JsonProperty(value = "description")
    private String description;
}
