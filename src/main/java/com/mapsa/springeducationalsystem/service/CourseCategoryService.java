package com.mapsa.springeducationalsystem.service;

import com.mapsa.springeducationalsystem.model.dto.CourseCategoryDto;

import java.util.List;

public interface CourseCategoryService {
    CourseCategoryDto createCourseCategory(CourseCategoryDto courseCategoryDto);
    void deleteCourseCategory (long courseCategoryId);
    void updateCourseCategory(CourseCategoryDto courseCategoryDto, long courseCategoryId);
    List<CourseCategoryDto> getAllCourseCategory ();
}
