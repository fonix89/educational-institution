package com.mapsa.springeducationalsystem.service;

import com.mapsa.springeducationalsystem.exception.NotFoundException;
import com.mapsa.springeducationalsystem.model.dto.CourseCategoryDto;
import com.mapsa.springeducationalsystem.model.entity.CourseCategory;
import com.mapsa.springeducationalsystem.model.mapper.CourseCategoryMapper;
import com.mapsa.springeducationalsystem.repo.CourseCategoryRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CourseCategoryServiceImpl implements CourseCategoryService{
    private final CourseCategoryRepo courseCategoryRepo;
    private final CourseCategoryMapper courseCategoryMapper;

    @Override
    public CourseCategoryDto createCourseCategory(CourseCategoryDto courseCategoryDto) {
        CourseCategory courseCategory = courseCategoryMapper.courseCategoryDtoToCourseCatergory(courseCategoryDto);
        courseCategoryRepo.save(courseCategory);
        return courseCategoryMapper.courseCategoryToCourseCategoryDto(courseCategory);
    }

    @Override
    public void deleteCourseCategory(long courseCategoryId) {
        if (courseCategoryRepo.findById(courseCategoryId).isPresent()){
            courseCategoryRepo.deleteById(courseCategoryId);
        }
        throw new NotFoundException("category not found");


    }

    @Override
    public void updateCourseCategory(CourseCategoryDto courseCategoryDto, long courseCategoryId) {
        CourseCategory courseCategory = courseCategoryRepo.findById(courseCategoryId).orElseThrow(NotFoundException::new);
        courseCategory.setCategoryName(courseCategoryDto.getCategoryName());
        courseCategoryRepo.save(courseCategory);

    }

    @Override
    public List<CourseCategoryDto> getAllCourseCategory() {
        return courseCategoryRepo
                .findAll()
                .stream()
                .map(courseCategoryMapper::courseCategoryToCourseCategoryDto)
                .collect(Collectors.toList());
    }
}
