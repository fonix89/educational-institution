package com.mapsa.springeducationalsystem.service;

import com.mapsa.springeducationalsystem.exception.NotFoundException;
import com.mapsa.springeducationalsystem.model.dto.LessonDto;
import com.mapsa.springeducationalsystem.model.entity.Lesson;
import com.mapsa.springeducationalsystem.model.mapper.LessonMapper;
import com.mapsa.springeducationalsystem.repo.LessonRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class LessonServiceImpl implements LessonService {

    private final LessonMapper lessonMapper;
    private final LessonRepo lessonRepo;


    @Override
    public LessonDto createALesson(LessonDto lessonDto) {
        Lesson lesson = lessonMapper.lessonDtoToLesson(lessonDto);
        Lesson createLesson = lessonRepo.save(lesson);
        return lessonMapper.lessonToLessonDto(createLesson);
    }

    @Override
    public void updateALesson(LessonDto lessonDto, long lessonId) {
        Lesson findLesson = lessonRepo.findById(lessonId).orElseThrow(NotFoundException::new);
        lessonMapper.updateLessonFromDto(lessonDto, findLesson);
        lessonRepo.save(findLesson);
    }

    @Override
    public void deleteALesson(long lessonId) {
        if (!lessonRepo.findById(lessonId).isPresent()){
            throw new NotFoundException("lesson not found");
        }
        lessonRepo.deleteById(lessonId);
    }

    @Override
    public LessonDto getALesson(long lessonId) {
        return lessonRepo
                .findById(lessonId)
                .map(LessonMapper.INSTANCE::lessonToLessonDto)
                .orElseThrow(NotFoundException::new);
    }

    @Override
    public List<LessonDto> getAllLessons() {
        return lessonRepo
                .findAll()
                .stream()
                .map(LessonMapper.INSTANCE::lessonToLessonDto)
                .collect(Collectors.toList());
    }



}
