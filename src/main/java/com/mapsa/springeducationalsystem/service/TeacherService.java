package com.mapsa.springeducationalsystem.service;

import com.mapsa.springeducationalsystem.model.dto.GetCoursesOfATeacherResponseDto;
import com.mapsa.springeducationalsystem.model.dto.TeacherDto;

import java.util.List;

public interface TeacherService {
    
     TeacherDto createATeacher(TeacherDto teacherDto);
     void updateATeacher(TeacherDto teacherDto, long teacherId);
     void deleteATeacher(long teacherId);
     TeacherDto getATeacher(long teacherId);
     TeacherDto getATeacherByUserName(String userName);
     List<TeacherDto> getAllTeachers();
     //void registerATeacherOnACourse(long courseId, long teacherId);
     GetCoursesOfATeacherResponseDto getCoursesOfATeacherResponseDto(long teacherId);
}
