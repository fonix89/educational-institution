package com.mapsa.springeducationalsystem.service;

import com.mapsa.springeducationalsystem.exception.NotFoundException;
import com.mapsa.springeducationalsystem.model.dto.CourseDto;
import com.mapsa.springeducationalsystem.model.entity.Course;
import com.mapsa.springeducationalsystem.model.entity.CourseCategory;
import com.mapsa.springeducationalsystem.model.entity.Lesson;
import com.mapsa.springeducationalsystem.model.entity.Teacher;
import com.mapsa.springeducationalsystem.model.mapper.CourseMapper;
import com.mapsa.springeducationalsystem.repo.CourseRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CourseServiceImpl implements CourseService {

    private final CourseRepo courseRepo;
    private final CourseMapper courseMapper;

    @Override
    public CourseDto createACourse(CourseDto courseDto) {
        Course course = courseMapper.courseDtoToCourse(courseDto);
        Course createCourse = courseRepo.save(course);
        return courseMapper.courseToCourseDto(createCourse);
    }

    @Override
    public void updateACourse(CourseDto courseDto, long courseId) {
        Course findCourse = courseRepo.findById(courseId).orElseThrow(NotFoundException::new);
        findCourse.setCourseCategory(new CourseCategory());
        findCourse.setTeacher(new Teacher());
        findCourse.setLesson(new Lesson());
        courseMapper.updateCourseFromDto(courseDto, findCourse);
        courseRepo.save(findCourse);
    }

    @Override
    public void deleteACourse(long courseId) {
        if (!courseRepo.findById(courseId).isPresent()){
            throw new NotFoundException("course not found.");
        }
        courseRepo.deleteById(courseId);
    }

    @Override
    public CourseDto getACourse(long courseId) {
        return courseRepo
                .findById(courseId)
                .map(courseMapper::courseToCourseDto)
                .orElseThrow(NotFoundException::new);
    }

    @Override
    public List<CourseDto> getAllCourses() {
        return courseRepo
                .findAll()
                .stream()
                .map(courseMapper::courseToCourseDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<CourseDto> getCoursesBetweenTwoDate(Date startDate, Date endDate) {
        List<Course> courses = courseRepo.findAllByStartDateGreaterThanEqualAndEndDateLessThanEqual(startDate, endDate);
        return courseMapper.courseListToCourseDtoList(courses);
    }

    @Override
    public List<CourseDto> getAllCoursesByLessonId(long lessonId) {
        List<Course> courses = courseRepo.findAllByLesson_Id(lessonId);
        return courseMapper.courseListToCourseDtoList(courses);
    }

}
