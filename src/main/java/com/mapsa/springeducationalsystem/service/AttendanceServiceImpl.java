package com.mapsa.springeducationalsystem.service;

import com.mapsa.springeducationalsystem.exception.NotFoundException;
import com.mapsa.springeducationalsystem.model.dto.AttendanceDto;
import com.mapsa.springeducationalsystem.model.dto.GetListOfStudentsAttendanceInACourseDto;
import com.mapsa.springeducationalsystem.model.entity.Attendance;
import com.mapsa.springeducationalsystem.model.entity.Course;
import com.mapsa.springeducationalsystem.model.mapper.AttendanceMapper;
import com.mapsa.springeducationalsystem.repo.AttendanceRepo;
import com.mapsa.springeducationalsystem.repo.CourseRepo;
import com.mapsa.springeducationalsystem.repo.StudentRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AttendanceServiceImpl implements AttendanceService {

    private final AttendanceRepo attendanceRepo;
    private final AttendanceMapper attendanceMapper;
    private final StudentRepo studentRepo;
    private final CourseRepo courseRepo;


    @Override
    public AttendanceDto createAttendance(AttendanceDto attendanceDto) {
        Attendance attendance = attendanceMapper.AttendanceDtoToAttendance(attendanceDto);
        attendanceRepo.save(attendance);
        return attendanceMapper.AttendanceToAttendanceDto(attendance);
    }

    @Override
    public void deleteAttendance(long attendanceId) {
        if (!attendanceRepo.findById(attendanceId).isPresent()) {
            throw new NotFoundException("student attendance not found.");
        }
        attendanceRepo.deleteById(attendanceId);
    }

    @Override
    public GetListOfStudentsAttendanceInACourseDto getListOfStudentsAttendanceInACourseDto(long courseId) {
        Course course = courseRepo.findById(courseId).orElseThrow(NotFoundException::new);
        return attendanceMapper.getListOfStudentsAttendanceInACourseConverter(course);
    }


    @Override
    public List<AttendanceDto> getAListOfAStudentAttendanceInACourse(long courseId, long studentId) {
        List<Attendance> attendances = attendanceRepo.findAllByCourse_IdAndStudent_Id(courseId, studentId);
        return attendanceMapper.getAListOfAStudentAttendanceInACourseConverter(attendances);
    }

}
