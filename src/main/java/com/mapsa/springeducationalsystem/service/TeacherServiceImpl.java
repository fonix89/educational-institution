package com.mapsa.springeducationalsystem.service;

import com.mapsa.springeducationalsystem.exception.NotFoundException;
import com.mapsa.springeducationalsystem.model.dto.GetCoursesOfATeacherResponseDto;
import com.mapsa.springeducationalsystem.model.dto.StudentDto;
import com.mapsa.springeducationalsystem.model.dto.TeacherDto;
import com.mapsa.springeducationalsystem.model.entity.Course;
import com.mapsa.springeducationalsystem.model.entity.Student;
import com.mapsa.springeducationalsystem.model.entity.Teacher;
import com.mapsa.springeducationalsystem.model.mapper.StudentMapper;
import com.mapsa.springeducationalsystem.model.mapper.TeacherMapper;
import com.mapsa.springeducationalsystem.repo.CourseRepo;
import com.mapsa.springeducationalsystem.repo.StudentRepo;
import com.mapsa.springeducationalsystem.repo.TeacherRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TeacherServiceImpl implements TeacherService{

    private final TeacherRepo teacherRepo;
    private final TeacherMapper teacherMapper;
    private final CourseRepo courseRepo;


    @Override
    public TeacherDto createATeacher(TeacherDto teacherDto) {
        Teacher teacher = teacherMapper.teacherDtoToTeacher(teacherDto);
        Teacher createTeacher = teacherRepo.save(teacher);
        return teacherMapper.teacherToTeacherDto(createTeacher);
    }

    @Override
    public void updateATeacher(TeacherDto teacherDto, long teacherId) {
        Teacher findTeacher = teacherRepo.findById(teacherId).orElseThrow(NotFoundException::new);
        teacherMapper.updateTeacherFromDto(teacherDto, findTeacher);
        teacherRepo.save(findTeacher);
    }

    @Override
    public void deleteATeacher(long teacherId) {
        if (!teacherRepo.findById(teacherId).isPresent()){
            throw new NotFoundException("teacher not found.");
        }
        teacherRepo.deleteById(teacherId);
    }

    @Override
    public TeacherDto getATeacher(long teacherId) {
        return teacherRepo
                .findById(teacherId)
                .map(teacherMapper::teacherToTeacherDto)
                .orElseThrow(NotFoundException::new);
    }

    @Override
    public TeacherDto getATeacherByUserName(String userName) {
        Teacher teacher = teacherRepo.findByUserName(userName);
        if (teacher != null){
            return teacherMapper.teacherToTeacherDto(teacher);
        }
        throw new NotFoundException("teacher not found.");
    }

    @Override
    public List<TeacherDto> getAllTeachers() {
        return teacherRepo
                .findAll()
                .stream()
                .map(teacherMapper::teacherToTeacherDto)
                .collect(Collectors.toList());
    }

//    @Override
//    public void registerATeacherOnACourse(long courseId, long teacherId) {
//        Course course = courseRepo.findById(courseId).orElseThrow(NotFoundException::new);
//        Teacher teacher = teacherRepo.findById(teacherId).orElseThrow(NotFoundException::new);
//        teacher.getTeacherCourseList().add(course);
//        teacherRepo.save(teacher);
//    }

    @Override
    public GetCoursesOfATeacherResponseDto getCoursesOfATeacherResponseDto(long teacherId) {
        Teacher teacher = teacherRepo.findById(teacherId).orElseThrow(NotFoundException::new);
        return teacherMapper.getCoursesOfATeacherResponseDtoConvert(teacher);
    }
}
