package com.mapsa.springeducationalsystem.service;

import com.mapsa.springeducationalsystem.model.dto.GetCoursesOfAStudentResponseDto;
import com.mapsa.springeducationalsystem.model.dto.GetListOfStudentsOfACourseResponseDto;
import com.mapsa.springeducationalsystem.model.dto.StudentDto;

import java.util.List;

public interface StudentService {

    StudentDto createAStudent(StudentDto studentDto);
    void updateAStudent(StudentDto studentDto, long studentId);
    void deleteAStudent(long studentId);
    StudentDto getAStudentById(long studentId);
    StudentDto getAStudentByUserName(String userName);
    List<StudentDto> getAllStudents();
    void registerStudentOnACourse(long studentId, long courseId);
    GetCoursesOfAStudentResponseDto getCoursesOfAStudent(long studentId);
    GetListOfStudentsOfACourseResponseDto getListOfStudentsOfACourse(long courseId);

}
