package com.mapsa.springeducationalsystem.service;

import com.mapsa.springeducationalsystem.model.dto.CourseDto;

import java.util.Date;
import java.util.List;

public interface CourseService {

    CourseDto createACourse(CourseDto courseDto);
    void updateACourse(CourseDto courseDto, long courseId);
    void deleteACourse(long courseId);
    CourseDto getACourse(long courseId);
    List<CourseDto> getAllCourses();
    List<CourseDto> getCoursesBetweenTwoDate(Date startDate, Date endDate);
    List<CourseDto> getAllCoursesByLessonId(long lessonId);

}
