package com.mapsa.springeducationalsystem.service;

import com.mapsa.springeducationalsystem.model.dto.LessonDto;

import java.util.List;

public interface LessonService {

    LessonDto createALesson(LessonDto lessonDto);
    void updateALesson(LessonDto lessonDto, long lessonId);
    void deleteALesson(long lessonId);
    LessonDto getALesson(long lessonId);
    List<LessonDto> getAllLessons();
}
