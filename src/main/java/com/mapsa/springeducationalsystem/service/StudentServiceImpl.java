package com.mapsa.springeducationalsystem.service;

import com.mapsa.springeducationalsystem.exception.NotFoundException;
import com.mapsa.springeducationalsystem.model.dto.*;
import com.mapsa.springeducationalsystem.model.entity.Course;
import com.mapsa.springeducationalsystem.model.entity.Student;
import com.mapsa.springeducationalsystem.model.mapper.StudentMapper;
import com.mapsa.springeducationalsystem.repo.CourseRepo;
import com.mapsa.springeducationalsystem.repo.StudentRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepo studentRepo;
    private final StudentMapper studentMapper;
    private final CourseRepo courseRepo;

    @Override
    public StudentDto createAStudent(StudentDto studentDto) {
        Student student = studentMapper.studentDtoToStudent(studentDto);
        Student createStudent = studentRepo.save(student);
        return studentMapper.studentToStudentDto(createStudent);
    }

    @Override
    public void updateAStudent(StudentDto studentDto, long studentId) {
        Student findStudent = studentRepo.findById(studentId).orElseThrow(NotFoundException::new);
        studentMapper.updateStudentFromDto(studentDto, findStudent);
        studentRepo.save(findStudent);
    }

    @Override
    public void deleteAStudent(long studentId) {
        if (!studentRepo.findById(studentId).isPresent()){
            throw new NotFoundException("student not found.");
        }
        studentRepo.deleteById(studentId);
    }

    @Override
    public StudentDto getAStudentById(long studentId) {
        return studentRepo
                .findById(studentId)
                .map(studentMapper::studentToStudentDto)
                .orElseThrow(NotFoundException::new);
    }

    @Override
    public StudentDto getAStudentByUserName(String userName) {
        Student student = studentRepo.findByUserName(userName);
        if (student != null){
            return studentMapper.studentToStudentDto(student);
        }
        throw new NotFoundException("student not found.");
    }

    @Override
    public List<StudentDto> getAllStudents() {
        return studentRepo
                .findAll()
                .stream()
                .map(studentMapper::studentToStudentDto)
                .collect(Collectors.toList());
    }

    @Override
    public void registerStudentOnACourse(long studentId, long courseId) {
        Student student = studentRepo.findById(studentId).orElseThrow(NotFoundException::new);
        Course course = courseRepo.findById(courseId).orElseThrow(NotFoundException::new);
        student.getStudentCourseList().add(course);
        studentRepo.save(student);
    }

    @Override
    public GetCoursesOfAStudentResponseDto getCoursesOfAStudent(long studentId) {
        Student findStudent = studentRepo.findById(studentId).orElseThrow(NotFoundException::new);
        return studentMapper.getCoursesOfAStudentResponseDtoConvert(findStudent);
    }

    @Override
    public GetListOfStudentsOfACourseResponseDto getListOfStudentsOfACourse(long courseId) {
        Course course = courseRepo.findById(courseId).orElseThrow(NotFoundException::new);
        List<StudentDto> studentDtoList = course
                .getStudentList()
                .stream()
                .map(studentMapper::studentToStudentDto)
                .collect(Collectors.toList());
        return new GetListOfStudentsOfACourseResponseDto(courseId,studentDtoList);
    }








}
