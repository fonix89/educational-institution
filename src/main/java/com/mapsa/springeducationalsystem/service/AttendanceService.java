package com.mapsa.springeducationalsystem.service;

import com.mapsa.springeducationalsystem.model.dto.AttendanceDto;
import com.mapsa.springeducationalsystem.model.dto.GetListOfStudentsAttendanceInACourseDto;

import java.util.List;

public interface AttendanceService {

    AttendanceDto createAttendance(AttendanceDto attendanceDto);
    void deleteAttendance(long attendanceId);
    GetListOfStudentsAttendanceInACourseDto getListOfStudentsAttendanceInACourseDto(long courseId);
    List<AttendanceDto> getAListOfAStudentAttendanceInACourse(long courseId, long studentId);
}
