package com.mapsa.springeducationalsystem.controller;

import com.mapsa.springeducationalsystem.model.dto.AttendanceDto;
import com.mapsa.springeducationalsystem.service.AttendanceService;
import com.mapsa.springeducationalsystem.service.AttendanceServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/")
public class AttendanceController {
    private final AttendanceServiceImpl attendanceServiceImpl;

    @PostMapping(value = "/attendance", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity create(@RequestBody AttendanceDto attendanceDto){
        return new ResponseEntity(attendanceServiceImpl.createAttendance(attendanceDto), HttpStatus.OK);
    }

    @DeleteMapping(value = "/attendance/{attendanceId}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity delete(@PathVariable long attendanceId){
        attendanceServiceImpl.deleteAttendance(attendanceId);
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping(value = "/attendance/student/{studentId}/course/{courseId}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAListOfAStudentAttendanceInACourse(@PathVariable long courseId, @PathVariable long studentId){
        return new ResponseEntity(attendanceServiceImpl.getAListOfAStudentAttendanceInACourse(courseId, studentId), HttpStatus.OK);
    }

    @GetMapping(value = "/attendance/course/{courseId}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getListOfStudentsAttendanceInACourse(@PathVariable long courseId){
        return new ResponseEntity(attendanceServiceImpl.getListOfStudentsAttendanceInACourseDto(courseId), HttpStatus.OK);
    }




}
