package com.mapsa.springeducationalsystem.controller;

import com.mapsa.springeducationalsystem.model.dto.CourseDto;
import com.mapsa.springeducationalsystem.service.CourseServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.print.attribute.standard.Media;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class CourseController {

    private final CourseServiceImpl courseServiceImpl;


    @PostMapping(value = "/course", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createCourse(@RequestBody CourseDto courseDto){
        return new ResponseEntity (courseServiceImpl.createACourse(courseDto),HttpStatus.OK);
    }

    @PatchMapping(value = "/course/{courseId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity update(@RequestBody CourseDto courseDto, @PathVariable long courseId){
        courseServiceImpl.updateACourse(courseDto, courseId);
        return new ResponseEntity(HttpStatus.OK);
    }

    @DeleteMapping(value = "/course/{courseId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity delete(@PathVariable long courseId){
        courseServiceImpl.deleteACourse(courseId);
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping(value = "/course/{courseId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity fetch(@PathVariable long courseId){
        return new ResponseEntity(courseServiceImpl.getACourse(courseId),HttpStatus.OK);
    }

    @ResponseBody()
    @GetMapping(value = "/course", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CourseDto> getAllCourses(){
        return new ArrayList<>(courseServiceImpl.getAllCourses());
    }

    @ResponseBody()
    @GetMapping(value = "/course/date", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public List<CourseDto> getListOfCoursesInATimeRang(@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") Date startDate, @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate){
        //System.out.println(startDate +" " + endDate);
        return new ArrayList<>(courseServiceImpl.getCoursesBetweenTwoDate(startDate, endDate));
    }

    @ResponseBody()
    @GetMapping(value = "/course/lesson/{lessonId}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public List<CourseDto> getListOfCoursesByLessonId(@PathVariable long lessonId){
        return new ArrayList<>(courseServiceImpl.getAllCoursesByLessonId(lessonId));
    }


}
