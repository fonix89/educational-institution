package com.mapsa.springeducationalsystem.controller;

import com.mapsa.springeducationalsystem.model.dto.TeacherDto;
import com.mapsa.springeducationalsystem.service.TeacherServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class TeacherController {

    private final TeacherServiceImpl teacherServiceImpl;

    @PostMapping(value = "/teacher", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity create(@RequestBody TeacherDto teacherDto){
        return new ResponseEntity(teacherServiceImpl.createATeacher(teacherDto), HttpStatus.OK);
    }

    @PatchMapping(value = "/teacher/{teacherId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity update(@RequestBody TeacherDto teacherDto,@PathVariable long teacherId){
        teacherServiceImpl.updateATeacher(teacherDto, teacherId);
        return new ResponseEntity(HttpStatus.OK);
    }

    @DeleteMapping(value = "/teacher/{teacherId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity delete(@PathVariable long teacherId){
        teacherServiceImpl.deleteATeacher(teacherId);
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping(value = "/teacher/{teacherId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity fetchById(@PathVariable long teacherId){
        return new ResponseEntity(teacherServiceImpl.getATeacher(teacherId),HttpStatus.OK);
    }

    @GetMapping(value = "/teacher", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity fetchByUserName(@RequestParam String userName){
        return new ResponseEntity(teacherServiceImpl.getATeacherByUserName(userName),HttpStatus.OK);
    }

    @ResponseBody
    @GetMapping(value = "/teachers", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<TeacherDto> fetchAllTeachers(){
        return new ArrayList<>(teacherServiceImpl.getAllTeachers());
    }

//    @PostMapping(value = "/teacher/{teacherId}/course/{courseId}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity registerTeacherOnACourse(@PathVariable long courseId, @PathVariable long teacherId){
//        teacherServiceImpl.registerATeacherOnACourse(courseId, teacherId);
//        return new ResponseEntity(HttpStatus.OK);
//    }

    @GetMapping(value = "/teacher/course/{teacherId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getCoursesOfATeacher(@PathVariable long teacherId){
        return new ResponseEntity(teacherServiceImpl.getCoursesOfATeacherResponseDto(teacherId), HttpStatus.OK);
    }

}
