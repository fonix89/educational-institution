package com.mapsa.springeducationalsystem.controller;

import com.mapsa.springeducationalsystem.model.dto.StudentDto;
import com.mapsa.springeducationalsystem.service.StudentServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.ArrayList;
import java.util.List;


@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class StudentController {

    private final StudentServiceImpl studentServiceImpl;

    @PostMapping(value = "/student", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity create(@RequestBody StudentDto studentDto){
        return new ResponseEntity(studentServiceImpl.createAStudent(studentDto), HttpStatus.OK);
    }

    @PatchMapping(value = "/student/{studentId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity update(@RequestBody StudentDto studentDto,@PathVariable long studentId){
        studentServiceImpl.updateAStudent(studentDto, studentId);
        return new ResponseEntity(HttpStatus.OK);
    }

    @DeleteMapping(value = "/student/{studentId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity delete(@PathVariable long studentId){
        studentServiceImpl.deleteAStudent(studentId);
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping(value = "/student/{studentId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity fetchById(@PathVariable long studentId){
        return new ResponseEntity(studentServiceImpl.getAStudentById(studentId),HttpStatus.OK);
    }

    @GetMapping(value = "/student", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity fetchByUserName(@RequestParam String userName){
        return new ResponseEntity(studentServiceImpl.getAStudentByUserName(userName),HttpStatus.OK);
    }

    @ResponseBody
    @GetMapping(value = "/students", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<StudentDto> fetchAllStudents(){
        return new ArrayList<>(studentServiceImpl.getAllStudents());
    }

    @PostMapping(value = "/student/{studentId}/course/{courseId}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity registerStudentOnACourse(@PathVariable long studentId, @PathVariable long courseId){
        studentServiceImpl.registerStudentOnACourse(studentId, courseId);
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping(value = "/student/{studentId}/course", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getCoursesOfAStudent(@PathVariable long studentId){
        return new ResponseEntity(studentServiceImpl.getCoursesOfAStudent(studentId), HttpStatus.OK);
    }

    @GetMapping(value = "/student/course/{courseId}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getListOfStudentsOfACourse(@PathVariable long courseId){
        return new ResponseEntity(studentServiceImpl.getListOfStudentsOfACourse(courseId), HttpStatus.OK);
    }





}
