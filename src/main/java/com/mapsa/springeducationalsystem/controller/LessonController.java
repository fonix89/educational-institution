package com.mapsa.springeducationalsystem.controller;

import com.mapsa.springeducationalsystem.model.dto.LessonDto;
import com.mapsa.springeducationalsystem.service.LessonServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class LessonController {

    private final LessonServiceImpl lessonServiceImpl;

    @PostMapping(value = "/lesson", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity create(@RequestBody LessonDto lessonDto){
        return new ResponseEntity(lessonServiceImpl.createALesson(lessonDto), HttpStatus.OK);
    }

    @PatchMapping(value = "/lesson/{lessonId}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity update(@RequestBody LessonDto lessonDto, @PathVariable long lessonId){
        lessonServiceImpl.updateALesson(lessonDto, lessonId);
        return new ResponseEntity(HttpStatus.OK);
    }

    @DeleteMapping(value = "/lesson/{lessonId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity delete(@PathVariable long lessonId){
        lessonServiceImpl.deleteALesson(lessonId);
        return new ResponseEntity(HttpStatus.OK);
    }

    @GetMapping(value = "/lesson/{lessonId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity fetchById(@PathVariable long lessonId){
        return new ResponseEntity(lessonServiceImpl.getALesson(lessonId),HttpStatus.OK);
    }

    @ResponseBody
    @GetMapping(value = "/lesson", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<LessonDto> fetchAllLessons(){
        return new ArrayList<>(lessonServiceImpl.getAllLessons());
    }





}
