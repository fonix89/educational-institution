package com.mapsa.springeducationalsystem.controller;

import com.mapsa.springeducationalsystem.model.dto.CourseCategoryDto;

import com.mapsa.springeducationalsystem.service.CourseCategoryServiceImpl;
import lombok.RequiredArgsConstructor;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.ArrayList;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1")
public class CourseCategoryController {

    private final CourseCategoryServiceImpl courseCategoryServiceImpl;

    @PostMapping(value = "/course/category", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity create(@RequestBody CourseCategoryDto courseCategoryDto){
        return new ResponseEntity(courseCategoryServiceImpl.createCourseCategory(courseCategoryDto), HttpStatus.OK);
    }

    @PatchMapping(value = "/course/category/{courseCategoryId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity update(@RequestBody CourseCategoryDto courseCategoryDto, @PathVariable long courseCategoryId){
        courseCategoryServiceImpl.updateCourseCategory(courseCategoryDto, courseCategoryId);
        return new ResponseEntity(HttpStatus.OK);
    }

    @DeleteMapping(value = "/course/category/{courseCategoryId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity delete(@PathVariable long courseCategoryId){
        courseCategoryServiceImpl.deleteCourseCategory(courseCategoryId);
        return new ResponseEntity(HttpStatus.OK);
    }

    @ResponseStatus
    @GetMapping(value = "/course/category", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CourseCategoryDto> getAllCategories(){
        return new ArrayList<>(courseCategoryServiceImpl.getAllCourseCategory());
    }
}
