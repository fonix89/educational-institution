package com.mapsa.springeducationalsystem.repo;

import com.mapsa.springeducationalsystem.model.entity.CourseCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseCategoryRepo extends JpaRepository<CourseCategory, Long> {
}
