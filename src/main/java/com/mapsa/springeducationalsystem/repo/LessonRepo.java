package com.mapsa.springeducationalsystem.repo;

import com.mapsa.springeducationalsystem.model.entity.Lesson;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface LessonRepo extends JpaRepository<Lesson, Long> {

    List<Lesson> findByLessonName(String lessonName);
}
