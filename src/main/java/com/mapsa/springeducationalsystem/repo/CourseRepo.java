package com.mapsa.springeducationalsystem.repo;

import com.mapsa.springeducationalsystem.model.entity.Course;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface CourseRepo extends JpaRepository<Course, Long> {

    //List<Course> findCoursesByStartDateAfterAndEndDateBefore(Date startDate, Date endDate);
    //List<Course> findCoursesByStartDateGreaterThanEqualAndEndDateLessThanEqual(Date startDate, Date endDate );
    List<Course> findAllByStartDateGreaterThanEqualAndEndDateLessThanEqual(Date startDate, Date endDate);
    List<Course> findAllByLesson_Id(long lessonId);











}
