package com.mapsa.springeducationalsystem.repo;

import com.mapsa.springeducationalsystem.model.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepo extends JpaRepository<Employee, Long> {
}
