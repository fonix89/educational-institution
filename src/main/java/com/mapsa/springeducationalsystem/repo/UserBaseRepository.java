package com.mapsa.springeducationalsystem.repo;

import com.mapsa.springeducationalsystem.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.w3c.dom.stylesheets.LinkStyle;

import java.util.List;

@NoRepositoryBean
public interface UserBaseRepository <T extends User> extends JpaRepository<T, Long> {

    T findByUserName(String userName);




}
