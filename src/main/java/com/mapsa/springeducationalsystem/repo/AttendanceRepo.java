package com.mapsa.springeducationalsystem.repo;

import com.mapsa.springeducationalsystem.model.dto.AttendanceDto;
import com.mapsa.springeducationalsystem.model.entity.Attendance;
import com.mapsa.springeducationalsystem.model.entity.Course;
import com.mapsa.springeducationalsystem.model.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AttendanceRepo extends JpaRepository<Attendance, Long> {

    List<Attendance> findAllByCourse_IdAndStudent_Id(long courseId, long studentId);

}
