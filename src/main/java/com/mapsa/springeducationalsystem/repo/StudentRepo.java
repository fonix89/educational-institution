package com.mapsa.springeducationalsystem.repo;

import com.mapsa.springeducationalsystem.model.entity.Student;

import javax.transaction.Transactional;

@Transactional
public interface StudentRepo extends UserBaseRepository<Student> {

}
