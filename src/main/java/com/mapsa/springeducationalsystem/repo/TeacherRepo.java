package com.mapsa.springeducationalsystem.repo;

import com.mapsa.springeducationalsystem.model.entity.Teacher;


import javax.transaction.Transactional;

@Transactional
public interface TeacherRepo extends UserBaseRepository<Teacher> {
}

